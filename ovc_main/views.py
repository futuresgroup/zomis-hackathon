from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
import workforceforms
from ovc_main import organisation_unit_forms
import json
from models import SetupGeorgraphy as locations
import organisation_registration
from organisation_registration import get_cwacs_in_ward
import workforce_registration
import child_reg_forms
import guardianforms
from django.template import RequestContext, loader
from django.shortcuts import render_to_response
from ovc_main.utils import fields_list_provider as field_provider, validators as validate_helper, geo_location,lookup_field_dictionary as lookup_field
from ovc_main.utils import auth_access_control_util
import beneficiary_registration
from django.contrib.auth.decorators import login_required
from ovc_main.core.workers.capture_utililty import get_upload_queue, get_downloads_queue, get_initial_data
from datetime import datetime
from datetime import date, time
import datetime
#from ovc_main.utils.geo_location import get_orgid_subunits_and_self
from ovc_auth import user_manager #import get_user_role_geo_org
#from django.utils import simplejson
#Simport page_definitions
from django.conf import settings
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from ovc_main.core.rest.serializers import *

# Create your views here.
@login_required
def index(request):
    from django.core.paginator import Paginator
    
    org_types = field_provider.get_list_of_organisation_types()
    org_list = organisation_registration.get_org_list(getJSON=True)
    
    pages = Paginator(org_list, 25)
    pageinfo = {'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': 1}
    return render(request, 'ovc_main/organisation_search.html', {'orgs_list':pages.page(1), 'pageinfo':pageinfo,'org_types':org_types})

@login_required(login_url='/')
def organisation_view(request):
    organisationid = request.GET['organisation_id']
    organisation = organisation_registration.load_org_from_id(organisationid)
    return render(request, 'ovc_main/organisation_view_readonly.html', {'organisation':organisation,})

@login_required(login_url='/')
def beneficiary_view(request):
    beneficiaryid = request.GET['beneficiary_id']
    beneficiary = beneficiary_registration.load_ben_from_id(beneficiaryid, True)
    
    print beneficiary.person_type
    
    if beneficiary.person_type == 'OVC':
        return render(request, 'ovc_main/ovc_child_view_readonly.html', {'beneficiary':beneficiary})
    else:
        return render(request, 'ovc_main/guardian_view_readonly.html', {'beneficiary':beneficiary})

def retrieve_geo_params(request, tmp):
    if 'districts' in request.POST:
        districts = request.POST.getlist('districts')
        tmp['districts'] = districts
    if 'wards' in request.POST:
        wards = request.POST.getlist('wards')
        tmp['wards'] = wards
    if 'communities' in request.POST:
        communities = request.POST.getlist('communities')
        tmp['communities'] = communities
        
    return tmp

@login_required(login_url='/')
def update_organisation(request):
    
    pagetitle = 'Update Organisation Unit'
    if request.GET:
        
        organisationid = request.GET['organisation_id']
        organisation = organisation_registration.load_org_from_id(organisationid)
        formfields = {'org_name': organisation.name,
                      'date_org_setup': organisation.date_operational,
                      'organisation_type': organisation.unit_type,
                      'legal_registration_type': organisation.legalregistrationtype,
                      'legal_registratiom_number': organisation.registrationnumber,
                      'land_phone_number': organisation.contact.land_phone_number,
                      'mobile_phone_number': organisation.contact.mobile_phone_number,
                      'email_address': organisation.contact.email_address,
                      'postal_address': organisation.contact.postal_address,
                      'physical_address': organisation.contact.physical_address,
                      'districts': organisation.locationserviceprovided['districts'],
                      'wards': organisation.locationserviceprovided['wards'],
                      'communities': organisation.locationserviceprovided['communities'],
                      'org_system_id': organisation.org_system_id,
                      'parent_unit': organisation.parent_organisation_id,
                      'org_never_existed': organisation.is_void,
                      'date_closed': organisation.date_closed}
        
        
        
        print formfields, 'our fields we will be printing'
        form = organisation_unit_forms.OrganisationRegistrationForm(request.user, formfields, is_update_page = True, **organisation.locationserviceprovided)
        return render(request, 'ovc_main/new_organisation.html', {'form':form, 'organisation':organisation, 'page_title':pagetitle})
    
    if request.POST:
        print request.POST, 'this is what we actually set out'
        organisationid = request.POST['org_system_id']
        print organisationid, 'in our fresh pist, organisation id'
        organisation = organisation_registration.load_org_from_id(organisationid)
        
        form = organisation_unit_forms.OrganisationRegistrationForm(request.user, request.POST, is_update_page = True)
        
        fields_to_bepulled_from_server = auth_access_control_util.filter_fields_for_display(request.user, organisation_unit_forms.permission_type_fields, organisation_id=organisationid)
        form.full_clean()
        tmp = form.cleaned_data
        tmp = retrieve_geo_params(request, tmp)
        print form.errors.items(), 'errors'
        print fields_to_bepulled_from_server, 'fields to be considered'
        temp_passed = auth_access_control_util.custom_form_is_valid(form.errors.items(), fields_to_bepulled_from_server)
        
        if (form.is_valid() or temp_passed) and form.is_custom_validation():            
            organisation_registration.update_organisation(tmp, request.user, changed_fields=form.changed_data, is_selective_update=True)
        #elif temp_passed  and form.is_custom_validation():
        #    organisation_registration.update_organisation(tmp, request.user, changed_fields=form.changed_data, is_selective_update=True)
            
        else:
            print 'all did not pass, in view of update page'
            form = organisation_unit_forms.OrganisationRegistrationForm(request.user, request.POST, is_update_page = True)
            return render(request, 'ovc_main/new_organisation.html', {'form':form, 'page_title':pagetitle,'organisation':organisation})
        return HttpResponseRedirect('/organisation/')


@login_required(login_url='/')
def new_organisation(request):
    form = organisation_unit_forms.OrganisationRegistrationForm(request.user)
    pagetitle = 'New Organisation Unit'
    if request.POST:
        print request.POST, 'VALUES  IN POST'
        form = organisation_unit_forms.OrganisationRegistrationForm(request.user,request.POST)
        
        if form.is_valid() and form.is_custom_validation():
            tmp = form.cleaned_data
            tmp = retrieve_geo_params(request, tmp)
            organisation_registration.create_organisation(tmp, request.user)
            
            return HttpResponseRedirect('/organisation/')
            
    return render(request, 'ovc_main/new_organisation.html', {'form':form,'page_title':pagetitle})

@login_required(login_url='/')
def new_workforce(request):
    #print request.POST
    form = workforceforms.WorkforceRegistrationForm(request.user)
    is_capture_app = settings.IS_CAPTURE_SITE
    if request.POST:
        #raise Exception('because we can')
        form = workforceforms.WorkforceRegistrationForm(request.user, request.POST)
        
        if form.is_valid():
            tmp = form.cleaned_data
            tmp = retrieve_geo_params(request, tmp)
            wfc = workforce_registration.save_new_workforce(tmp, request.user)  
            success = ''
            if wfc.direct_services:
                if wfc.direct_services == '1':
                    success = ' Workforce saved successfully'
                else:
                    success = ' User saved successfully'
            #return HttpResponseRedirect('/workforce/wfcsearch/')
            from django.core.paginator import Paginator

            wfc_types = field_provider.get_Workforce_Type(search=False)
            wfc_list = workforce_registration.get_wfc_list(getJSON=True,user=request.user)
            pages = Paginator(wfc_list, 25)
            pageinfo = {'pageinfo':'pageinfo',
                        'pagescount':pages.num_pages,
                        'total_records':pages.count,
                        'pagenumber': 1,
                        'success':success}

            return render(request, 'ovc_main/workforce_search.html', {'wfc_list':pages.page(1), 'pageinfo':pageinfo,'wfc_types':wfc_types,'is_capture_app':is_capture_app,})
    
    pagetitle = 'New Workforce'
    return render(request, 'ovc_main/new_workforce.html', {'form':form, 'page_title':pagetitle})

    
@login_required(login_url='/')
def workforce_index(request):
    from django.core.paginator import Paginator
    
    wfc_types = field_provider.get_Workforce_Type(search=False)
    wfc_list = workforce_registration.get_wfc_list(getJSON=True,user=request.user)

    pages = Paginator(wfc_list, 25)
    pageinfo = {'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': 1}
    is_capture_app = settings.IS_CAPTURE_SITE
    return render(request, 'ovc_main/workforce_search.html', {'wfc_list':pages.page(1), 'pageinfo':pageinfo,'wfc_types':wfc_types,'user':request.user,'is_capture_app':is_capture_app})

@login_required(login_url='/')
def wfc_search_results(request):
    from django.core.paginator import Paginator
    
    searchtokenstring = request.GET['searchterm']

    itemsperpage = 25
    if 'itemsperpage' in request.GET:
        itemsperpage = request.GET['itemsperpage']
    
    wfc_type = None
    if 'wfctype' in request.GET:
        wfc_type = request.GET['wfctype']
        
    pagenumber = 1
    if 'pagenumber' in request.GET:
        pagenumber = request.GET['pagenumber']
        
    searchtokens = searchtokenstring.strip().split(' ')
    wfc_list = workforce_registration.get_wfc_list(cleantokens(searchtokens), getJSON=True, wfc_type=wfc_type, user = request.user)
    
    if int(itemsperpage) == -1:
        pages = Paginator(wfc_list, 0)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':1,
                'total_records':len(wfc_list),
                'pagenumber': 1}
        todisplay = wfc_list
    else:
        pages = Paginator(wfc_list,itemsperpage)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': pagenumber}
        todisplay = pages.page(pagenumber).object_list


    return HttpResponse(json.dumps((todisplay +[pageinfo] )))

@login_required(login_url='/')
def workforce_view(request):
    person_id = request.GET['person_id_int']
    user = request.user
    workforce = workforce_registration.load_wfc_from_id(person_id,user)
    
    sec = get_security_attributes(user,workforce)

    return render(request, 'ovc_main/workforce_view_readonly.html', {'workforce':workforce,'sec':sec})

def get_security_attributes(user=None,workforce=None):
    user_can_edit = 'None'
    user_can_view = 'None'
    user_can_view_cursor = 'None'
    user_can_assign_role = 'None'
    if user:
        if user.is_superuser:
            user_can_edit = 'text'
            user_can_view = 'table_row_clickable'
            user_can_view_cursor = 'cursor:pointer'
            user_can_assign_role = 'text'
        else:
            if user.has_perm('auth.wkf view'):
                user_can_view = 'table_row_clickable'
                user_can_view_cursor = 'cursor:pointer'

            ''' In lookup, show the 'file update' button depending on permissions '''
            ''' 1. any cross cutting 'file update' permission for workforce(15,16 or 17) or ''' 
            user_has_cross_cutn_perms = user.has_perm('auth.update wkf contact') or user.has_perm('auth.update wkf general') or user.has_perm('auth.update wkf special')

            ''' 2. user is related to the selected workforce member and has specific 'file update' permissions (5 or 6), or ''' 
            wfc_related_to_user = workforce_registration.wfc_and_user_related(workforce,user)
            user_has_specific_update_per = user.has_perm('auth.update wkf contact rel') or user.has_perm('auth.update wkf general rel')
            related_and_has_update_perms = wfc_related_to_user and user_has_specific_update_per

            ''' 3. the user is the selected workforce member and has permission 37 ''' 
            if user.reg_person:
                wfc_is_logged_in_user_with_perm = user.reg_person.pk == workforce.id_int and user.has_perm('auth.update wkf contact own')
                if user_has_cross_cutn_perms or related_and_has_update_perms or wfc_is_logged_in_user_with_perm:
                    user_can_edit = 'text'

            ''' if user has crosscutting permission 32 or 33, then they can allocate roles '''
            #print user.has_perm('auth.role management'),'user.has_perm(\'auth.role management\')'
            #print user.has_perm('auth.assign role non org spec'),'user.has_perm(\'auth.assign role non org spec\')'
            if user.has_perm('auth.role management') or user.has_perm('auth.assign role non org spec'):
                user_can_assign_role = 'text'

            ''' If they have org specific permissions 10 and 31, then they can view person based on some factors '''
            if user.has_perm('auth.role assign org rel'):
                wfc_related_to_user = workforce_registration.wfc_and_user_related(workforce,user)
                if wfc_related_to_user:
                    user_can_assign_role = 'text'

            if user.has_perm('auth.assign role org spec'):
                belongs_to_same_org = workforce_registration.user_and_wfc_same_org(workforce,user)
                if belongs_to_same_org:
                    user_can_edit = 'text'

    sec = {'user_can_edit':user_can_edit,'user_can_view':user_can_view,'user_can_view_cursor':user_can_view_cursor,'user_can_assign_role':user_can_assign_role,}
    return sec
    
@login_required(login_url='/')
def workforce_update(request):
    is_related_to_user = False
    is_logged_in_user = False
    user = request.user
    if request.GET:
        personid = request.GET['person_id']
        workforce = workforce_registration.load_wfc_from_id(personid,user)
        tmp_direct_services = '1'
        if workforce.workforce_id == lookup_field.empty_workforce_id:
            tmp_direct_services = '2'
        if workforce.date_of_birth:
            workforce.date_of_birth = workforce.date_of_birth.strftime(validate_helper.date_input_format)

        is_related_to_user = workforce_registration.wfc_and_user_related(workforce,user)
        if user.reg_person:
            if user.reg_person.pk == int(personid):
                is_logged_in_user = True
        
        formfields = {'nrc': workforce.national_id,
                      'wfc_system_id': personid,
                      'workforce_id': workforce.workforce_id,
                      'first_name': workforce.first_name,
                      'last_name': workforce.surname,
                      'other_names': workforce.other_names,
                      'sex': workforce.sex_id,
                      'date_of_birth': workforce.date_of_birth,
                      'workforce_type': workforce.person_type_id,
                      'man_number': workforce.man_number,
                      'ts_number': workforce.ts_number,
                      'sign_number': workforce.sign_number,
                      'direct_services': tmp_direct_services,                      
                      'org_units': workforce.org_units,
                      'districts': workforce.geo_location['districts'],
                      'wards': workforce.geo_location['wards'],
                      'communities': workforce.geo_location['communities'],
                      'designated_phone_number': workforce.contact.designated_phone_number,
                      'other_mobile_number': workforce.contact.other_mobile_number,
                      'email_address': workforce.contact.email_address,
                      'physical_address': workforce.contact.physical_address,
                      'steps_ovc_number': workforce.steps_ovc_number,
                      'wfc_system_id': workforce.id_int,
                      'org_data_hidden': workforce.org_data_hidden,
                      'is_related_to_user': is_related_to_user,
                      'is_logged_in_user': is_logged_in_user   ,
                      'primary_org_id': workforce.primary_org_id                   
                      }
        select_fields_radio = 'checked'
        pagetitle = 'Update Workforce Unit'
        form = workforceforms.WorkforceRegistrationForm(request.user, formfields, is_update_page = True)
        return render(request, 'ovc_main/new_workforce.html', {'form':form, 'workforce':workforce, 'page_title':pagetitle,'select_fields_radio':select_fields_radio})

    if request.POST:
        #print request.POST,'request.POST'
        pagetitle = 'Update Workforce Unit'
        tmp_current_workforce_id = request.POST['wfc_system_id']
        tmp_org_workforce_obj = workforce_registration.load_wfc_from_id(tmp_current_workforce_id,request.user)
        form = workforceforms.WorkforceRegistrationForm(request.user, request.POST, is_update_page = True)
        
        fields_to_bepulled_from_server = auth_access_control_util.filter_fields_for_display(request.user, workforceforms.fields_by_perms, owner_id=tmp_current_workforce_id, organisation_id=tmp_org_workforce_obj.primary_org_id)
        form.full_clean()
        temp_passed = auth_access_control_util.custom_form_is_valid(form.errors.items(), fields_to_bepulled_from_server)
        
        received_data = form.cleaned_data
        received_data = retrieve_geo_params(request, received_data)
        person_id = 0
        if received_data.has_key('wfc_system_id'):
            person_id = received_data['wfc_system_id']
        national_id = ''
        if received_data.has_key('nrc'):
            national_id = received_data['nrc']
        edit_mode_hidden = '1'
        if received_data.has_key('edit_mode_hidden'):
            edit_mode_hidden = received_data['edit_mode_hidden']
        #print form.errors.items(),'form.errors.items()'
        select_death_radio = ''
        select_fields_radio = 'checked'
        #If we are setting a user's date of death, only the date of death validation matters
        if edit_mode_hidden == '2' and not temp_passed:
            for field, value in form.errors.items():
                if field == 'date_of_death':
                    temp_passed = False
                    select_death_radio = 'checked'
                    select_fields_radio = ''
        #If we are deactivating user and custom validation fails, we don't really mind coz none of those fields matter anyway
        if edit_mode_hidden == '3' and not temp_passed:
            temp_passed = True
            
        if user.reg_person:
            if user.reg_person.pk == int(person_id):
                is_logged_in_user = True
        
        #Dont allow self deactivation and setting own date of death
        #if (edit_mode_hidden == '3' or edit_mode_hidden == '2') and is_logged_in_user:
            #temp_passed = False
            
        wfc = None
        #print temp_passed,'temp_passed'
        if form.is_valid() and False:  #making it fail at all times so we handle everything
            wfc = workforce_registration.update_workforce(received_data, person_id, request.user, changed_fields=form.changed_data)
        elif temp_passed:
            wfc = workforce_registration.update_workforce(received_data, person_id, request.user, changed_fields=form.changed_data, is_selective_update=True)
        else:
            wfc = workforce_registration.load_wfc_from_id(person_id,request.user)
            post = request.POST.copy()
            post['primary_org_id'] = wfc.primary_org_id
            form = workforceforms.WorkforceRegistrationForm(request.user, post, is_update_page = True)
            failed_save = True
            return render(request, 'ovc_main/new_workforce.html', {'form':form, 'workforce':wfc, 'page_title':pagetitle, 'select_death_radio':select_death_radio, 'select_fields_radio':select_fields_radio, 'failed_save':failed_save})
                    
        success = ''
        #print wfc.direct_services,'wfc.direct_services'
        if wfc.direct_services:
            if wfc.workforce_id == lookup_field.empty_workforce_id:
                success = ' User updated successfully'
            else:
                success = ' Workforce updated successfully.'
        from django.core.paginator import Paginator

        wfc_types = field_provider.get_Workforce_Type(search=False)
        wfc_list = workforce_registration.get_wfc_list(getJSON=True,user=request.user)
        pages = Paginator(wfc_list, 25)
        pageinfo = {'pageinfo':'pageinfo',
                    'pagescount':pages.num_pages,
                    'total_records':pages.count,
                    'pagenumber': 1,
                    'success':success}
        return render(request, 'ovc_main/workforce_search.html', {'wfc_list':pages.page(1), 'pageinfo':pageinfo,'wfc_types':wfc_types})

@login_required(login_url='/')       
def update_beneficiary(request):
    print 'outside get mode'
    if request.GET:
        personid = request.GET['beneficiary_id']
        beneficiary = beneficiary_registration.load_ben_from_id(personid)
        
        guardian_data = 'first_time'
        g_chnge_date = None
        for grd in beneficiary.guardian:
            g_chnge_date = grd.datelinked
        
        today = datetime.now().date()#datetime.datetime.date()#.today()
        dpff = datetime.strptime(str(today), '%Y-%m-%d')
        
        date_paper_form = dpff.strftime('%d-%B-%Y')
        
        print 'loc_change', beneficiary.location_change_date
                
        formfields = {
                      'nrc': beneficiary.national_id,
                      'pk_id': beneficiary.pk_id,
                      'beneficiary_id': beneficiary.beneficiary_id,
                      'first_name': beneficiary.first_name,
                      'last_name': beneficiary.last_name,
                      'other_name': beneficiary.other_name,
                      'sex': beneficiary.sex_id,
                      'date_of_birth': beneficiary.date_of_birth,
                      'birth_cert_num': beneficiary.birth_cert_num,
                      'STEPS_OVC_number': beneficiary.steps_ovc_number,
                      'nrc': beneficiary.national_id,
                      'guardians': beneficiary.guardian,
                      'mobile_phone_number': beneficiary.contact.mobile_phone_number,
                      'email_address': beneficiary.contact.email_address,
                      'physical_address': beneficiary.contact.physical_address,
                      'ben_district': beneficiary.residence.district,
                      'ben_ward': beneficiary.residence.ward,
                      'community': beneficiary.residence.community,
                      'res_id_hidden': beneficiary.residence.res_id,
                      'workforce_member': beneficiary.workforce_member,
                      'date_paper_form_filled': beneficiary.date_paper_form_filled,
                      'guardians_hidden': guardian_data,
                      'guard_change_date': g_chnge_date,
                      'location_change_date': beneficiary.location_change_date.strftime('%d-%B-%Y'),
                      'date_paper_form_filled': date_paper_form,
                      'residential_institution': beneficiary.residence.residential_institution,
                      }
        
        page_to_display = ''
        if beneficiary.person_type == 'OVC':
            pagetitle = 'Update OVC Child'
            form = child_reg_forms.ChildRegistrationForm(request.user,formfields, is_update_page = True)
            page_to_display = 'ovc_main/new_child.html'
        if beneficiary.person_type == 'OVC guardian':
            pagetitle = 'Update OVC Guardian'
            form = guardianforms.GuardianRegistrationForm(request.user,formfields, is_update_page = True)
            page_to_display = 'ovc_main/new_guardian.html'
        return render(request, page_to_display, {'form':form, 'beneficiary':beneficiary, 'page_title':pagetitle})

    if request.POST:
        form_for_id = child_reg_forms.ChildRegistrationForm(request.user,request.POST, is_update_page = True)
        
        person_id = form_for_id.data['pk_id']
        person_type = None
        
        if person_id:
            person_type = field_provider.get_person_type(person_id)
        
        pagetitle = 'Update Beneficiary'
        form = None
        
        permission_type_fields = None
        
        print 'our user',request.user
        if person_type == 'OVC':
            form = child_reg_forms.ChildRegistrationForm(request.user,request.POST, is_update_page = True)
            permission_type_fields = child_reg_forms.permission_type_fields
        else:
            form = guardianforms.GuardianRegistrationForm(request.user, request.POST, is_update_page = True)
            permission_type_fields = guardianforms.permission_type_fields
        
        fields_to_bepulled_from_server = auth_access_control_util.filter_fields_for_display(request.user, permission_type_fields)
        form.full_clean()
        tmp = form.cleaned_data
        tmp = retrieve_geo_params(request, tmp)
        print form.errors.items(), 'errors'
        print fields_to_bepulled_from_server, 'fields to be considered'
        temp_passed = auth_access_control_util.custom_form_is_valid(form.errors.items(), fields_to_bepulled_from_server)
        
        if (form.is_valid() or temp_passed):# and form.is_custom_validation():          
            received_data = form.cleaned_data
            beneficiary_registration.update_ben(received_data, person_id, request.user, person_type, is_selective_update=True)
            #organisation_registration.update_organisation(tmp, request.user, changed_fields=form.changed_data, is_selective_update=True)
            
            from django.core.paginator import Paginator
    
            ben_list = []
            ben_types = field_provider.get_list_of_ben_types()
            ben_list = beneficiary_registration.load_beneficiaries(getJSON=True)
            
            success = None
            
            if person_type == 'OVC':
                success = 'Child updated successfully'
            else:
                success = 'Guardian updated successfully'
            
            pages = Paginator(ben_list, 25)
            pageinfo = {'pageinfo':'pageinfo',
                        'pagescount':pages.num_pages,
                        'total_records':pages.count,
                        'pagenumber': 1,
                        'success': success
                        }
            
            return render(request, 'ovc_main/beneficiary_search.html', {'bens_list':ben_list, 'pageinfo':pageinfo, 'ben_types':ben_types, }) 
                
        else:
            print form.errors
            if person_type == 'OVC':
                page_to_display = 'ovc_main/new_child.html'
            else:
                page_to_display = 'ovc_main/new_guardian.html'
                                
            return render(request, page_to_display, {'form':form, 'page_title':pagetitle})
        
        
def listofwards(request):
    if request.method == 'GET':
        
        print request.GET
        
        district = request.GET['district_id']

        wardstoreturn = {}
        if district:
            
            constituencies =  locations.objects.filter(parent_area_id=district)
            for constituency in constituencies:
                for ward in locations.objects.filter(parent_area_id=constituency.area_id):
                    wardstoreturn[ward.area_id] = constituency.area_name+'-'+ward.area_name
        
        return HttpResponse(json.dumps(wardstoreturn))

def list_of_wards_in_district(request):
    dist_id = request.GET['district_id']
    wards = None
    wards = field_provider.get_list_children_from_parent_area_id(dist_id)
    
    return HttpResponse(json.dumps(list_of_wards))


def list_of_cwacs_in_ward(request):

    wardid = request.GET['ward_id']
    cwacs = {}
    
    if wardid:
        tmpcwacs = get_cwacs_in_ward(wardid)
        for cwac in tmpcwacs:
            cwacs[cwac.org_system_id] = '%s %s' % (cwac.org_id, cwac.name)
            
    return HttpResponse(json.dumps(cwacs))

@login_required(login_url='/') 
def org_autocompletion_list(request):
    if request.method == 'GET':
        text = request.GET['org_name']
        user = request.user
        geo_id_with_wra_role = []

        user_role_geo_org = user_manager.get_user_role_geo_org(user)
        for role_geo_org in user_role_geo_org:
            if role_geo_org.group.group_name == 'Registration assistant':
                geo_id_with_wra_role.append(role_geo_org.org_unit.pk)
        org_units = field_provider.get_list_of_organisations(text,geo_id_with_wra_role,user)
        return HttpResponse(json.dumps(org_units))

def ben_autocompletion_list(request):
    if request.method == 'GET':
        print request.GET
        text = request.GET['ben_name']
        ben_info = field_provider.get_list_of_beneficiaries(text, 'guardian')
        
        return HttpResponse(json.dumps(ben_info))
    
def get_guardian_contacts(request):
    if request.method == 'GET':
        print request.GET
        grd_id = request.GET['guardian_id']
        ben_info = field_provider.get_guardian_contacts(grd_id)
        print ben_info
        return HttpResponse(json.dumps(ben_info))

def ben_wfc_autocompletion_list(request):
    if request.method == 'GET':
        print 'wfc autocomp'
        print request.GET
        text = request.GET['wfc_name']
        ben_info = field_provider.get_list_of_beneficiaries(text, 'workforce')
        
        return HttpResponse(json.dumps(ben_info))

def ben_res_autocompletion_list(request):
    if request.method == 'GET':
        print request.GET
        text = request.GET['ins_name']
        ben_info = field_provider.get_list_of_residentials(text)
        
        print 'ben information'
        print ben_info
        
        return HttpResponse(json.dumps(ben_info))

def org_search_results(request):
    from django.core.paginator import Paginator
    
    print request.GET, 'checking what was passed'
    searchtokenstring = request.GET['searchterm']
    
    search_all = False
    if 'search_all' in request.GET:
        search_all = request.GET['search_all']
        if search_all.lower() == 'true':
            search_all = True
        else:
            search_all = False
    
    itemsperpage = 25
    if 'itemsperpage' in request.GET:
        itemsperpage = request.GET['itemsperpage']
    
    org_type = None
    if 'orgtype' in request.GET:
        org_type = request.GET['orgtype']
        
    pagenumber = 1
    if 'pagenumber' in request.GET:
        pagenumber = request.GET['pagenumber']
        
    searchtokens = searchtokenstring.strip().split(' ')
    org_list = organisation_registration.get_org_list(cleantokens(searchtokens), getJSON=True, org_type=org_type, search_all_orgs=search_all)
    
    print 'these are the items by page', itemsperpage
    if int(itemsperpage) == -1:
        pages = Paginator(org_list, 0)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':1,
                'total_records':len(org_list),
                'pagenumber': 1}
        todisplay = org_list
    else:
        pages = Paginator(org_list,itemsperpage)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': pagenumber}
        todisplay = pages.page(pagenumber).object_list


    return HttpResponse(json.dumps((todisplay +[pageinfo] )))


def ben_search_results(request):
    from django.core.paginator import Paginator
    
    print request.GET, 'checking what was passed beneficiary'
    searchtokenstring = request.GET['searchterm']
    
    itemsperpage = 25
    if 'itemsperpage' in request.GET:
        itemsperpage = request.GET['itemsperpage']
    
    ben_type = None
    if 'bentype' in request.GET:
        ben_type = request.GET['bentype']
        
    pagenumber = 1
    if 'pagenumber' in request.GET:
        pagenumber = request.GET['pagenumber']
        
    searchtokens = searchtokenstring.strip().split(' ')
    ben_list = beneficiary_registration.load_beneficiaries(token = searchtokenstring, ben_type = ben_type, getJSON=True) #organisation_registration.get_org_list(cleantokens(searchtokens), getJSON=True, org_type=org_type)
    
    print 'these are the items per page', itemsperpage
    if int(itemsperpage) == -1:
        pages = Paginator(ben_list, 0)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':1,
                'total_records':len(ben_list),
                'pagenumber': 1}
        todisplay = ben_list
    else:
        pages = Paginator(ben_list,itemsperpage)
        pageinfo = {'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': pagenumber}
        todisplay = pages.page(pagenumber).object_list
        
        print 'page information:' 
        print pageinfo
        print 'page toDisplay:' 
        print todisplay
        print 'page json:' 
        print json.dumps((todisplay +[pageinfo] ))

    return HttpResponse(json.dumps((todisplay +[pageinfo] )))


def cleantokens(tokens):
    '''
    get rid of of empty tokens
    method should be improved on
    '''
    toreturn = []
    for token in tokens:
        if token:
            toreturn.append(token.strip())
    if toreturn:
        return toreturn
    return None
@login_required(login_url='/')
def new_guardian_reg(request):
    print request.POST
    form = guardianforms.GuardianRegistrationForm(request.user)
    
    if request.POST:
        form = guardianforms.GuardianRegistrationForm(request.user,request.POST)
        
        if form.is_valid():
            print request.user.id
            print form.cleaned_data
            beneficiary_registration.save_beneficiary(form.cleaned_data, request.user, 'guardian')    
            
            from django.core.paginator import Paginator
    
            ben_list = []
            ben_types = field_provider.get_list_of_ben_types()
            ben_list = beneficiary_registration.load_beneficiaries(getJSON=True)
            
            success = 'Guardian saved successfully'
            
            pages = Paginator(ben_list, 25)
            pageinfo = {'pageinfo':'pageinfo',
                        'pagescount':pages.num_pages,
                        'total_records':pages.count,
                        'pagenumber': 1,
                        'success': success
                        }
            
            return render(request, 'ovc_main/beneficiary_search.html', {'bens_list':ben_list, 'pageinfo':pageinfo, 'ben_types':ben_types, }) 
            #return HttpResponseRedirect('/beneficiary/ben_search')
    
    return render(request, 'ovc_main/new_guardian.html', {'form':form})
@login_required(login_url='/')
def new_child_reg(request):
    print request.POST
    form = child_reg_forms.ChildRegistrationForm(request.user)
    
    if request.POST:
        form = child_reg_forms.ChildRegistrationForm(request.user,request.POST)
        
        print 'before valid'
        if form.is_valid():
            print 'after valid'
            print request.user.id
            print form.cleaned_data    
            beneficiary_registration.save_beneficiary(form.cleaned_data, request.user, 'ovc')
            
            from django.core.paginator import Paginator
    
            ben_list = []
            ben_types = field_provider.get_list_of_ben_types()
            ben_list = beneficiary_registration.load_beneficiaries(getJSON=True)
            
            success = 'Child saved successfully'
            
            pages = Paginator(ben_list, 25)
            pageinfo = {'pageinfo':'pageinfo',
                        'pagescount':pages.num_pages,
                        'total_records':pages.count,
                        'pagenumber': 1,
                        'success': success
                        }
            
            return render(request, 'ovc_main/beneficiary_search.html', {'bens_list':ben_list, 'pageinfo':pageinfo, 'ben_types':ben_types, }) 
        
            #return HttpResponseRedirect('/beneficiary/ben_search/')
        else: print form.errors
    
    return render(request, 'ovc_main/new_child.html', {'form':form})

def search_beneficiary(request):
    
    from django.core.paginator import Paginator
    
    ben_list = []
    ben_types = field_provider.get_list_of_ben_types()
    ben_list = beneficiary_registration.load_beneficiaries(getJSON=True)
    
    pages = Paginator(ben_list, 25)
    pageinfo = {'pageinfo':'pageinfo',
                'pagescount':pages.num_pages,
                'total_records':pages.count,
                'pagenumber': 1}
    
    return render(request, 'ovc_main/beneficiary_search.html', {'bens_list':ben_list, 'pageinfo':pageinfo, 'ben_types':ben_types, })         

                
            
#def process_page(page_fields):
#    page_def = page_definitions[page_fields]
#    
#    try:
#        page_def(page_fields)
#    except:
#        return 'failed'
#    
#    return 'success'
    
   

@login_required(login_url='/')
def capture_admin(request):
    initial_data = get_initial_data()
    return render(request, 'ovc_main/capture_administration.html',initial_data)

#initiated from capture site
@login_required(login_url='/')
def start_upload(request):
    print 'Start upload'
    uploads = get_upload_queue()
    
    #start the upload process
    from rest_client import upload
    upload.delay()
    return HttpResponse(json.dumps((uploads)))

@api_view(['GET', 'POST'])
def process_upload(request, format=None):
    print "Received the message from client"
    print request.method,'request.method'
    serializer = FormsSerializer(data=request.data)
    if request.method == 'POST':
        print request.data,'request.data'
        print serializer.is_valid(),'serializer.is_valid()'
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@login_required(login_url='/')
def start_download(request):
    '''print 'Start download'
    downloads = get_downloads_queue()
    
    #start the download process    
    from rest_client import download
    download.delay()
    print "YOLO"
    return HttpResponse(json.dumps((downloads)))'''
    print 'Start download'

    if request.method == 'GET':
        section = request.GET['section']
        #section.encode('ascii','ignore')
        params = {'section':section}
        print params, 'params'
        
        from rest_client import download
        download.delay(params)
        print 'YO we got here'
        downloads = get_downloads_queue()
        print downloads,'downloads'
    return HttpResponse(json.dumps((downloads)))

@api_view(['GET'])
def download(request, format=None):
    """
    Download sections for the server's database
    
    """
    #TODO error handling
    print 'Downloading items'
    from models import *
    if request.method == 'GET':
        model_id = request.GET['type']
        model_class_str = ''
        serialiser_class_str = ''
        go_on = True
        for v in lookup_field.download_sections.values():
            if not go_on:
                break
            our_models = v['models']
            for model_info in our_models:
                if model_info['id'] == model_id:
                    model_class_str = model_info['model']
                    serialiser_class_str = model_info['serializer']
                    go_on = False
                    break
        model_class = globals()[model_class_str]
        serialiser_class = globals()[serialiser_class_str]
        
        objects = model_class.objects.filter()
        serializer = serialiser_class(objects, many=True)
        return Response(serializer.data)
                    
'''
@api_view(['GET'])
def get_persons_list(request, format=None):
    """
    List all users.
    """
    print 'Get persons'
    if request.method == 'GET':
        print 'GET'
        persons = RegPerson.objects.filter(is_void=False, date_of_death=None)
        serializer = RegPersonSerializer(persons, many=True)
        print serializer.data,'data serialised'
        return Response(serializer.data)
'''