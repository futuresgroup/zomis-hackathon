$(document).ready(
        function () {
            //BEGIN PLUGINS SELECT2
            //$("#id_date_of_birth").onSelect(function(date,inst) {
            //	alert(date);
            //});
            $('.select2-category').select2({
                placeholder: "Select an option",
                allowClear: true
            });
            $(".select2-tagging-support").select2({
                tags: ["red", "green", "blue", "yellow", "green"]
            });
            $(".select2-multi-value").select2();
            $('.select2-size').select2({
                placeholder: "Select an option",
                allowClear: true
            });

            //$(".table_row_clickable").click(function(){
            $(".table").on("click", ".table_row_clickable", function () {

                window.location = $(this).attr("href");
            });

            hidelocationcontrols = function (initialrun)
            {

                if (typeof ($("#id_organisation_type").val()) == 'undefined')
                {
                    on_org_type_change();
                    $("#ward_location, #community_location, #district_location").show();
                }
                else {
                    on_org_type_change();
                    //alert(initialrun);
                    if (initialrun == false)
                    {
                        $("#districts").multiSelect("deselect_all");
                        $("#districts").multiSelect("refresh");

                    }

                    $("#ward_location, #community_location, #district_location").hide();
                    $("#div_id_legal_registration_type").hide();
                    $("#div_id_legal_registratiom_number").hide();
                }

            };
            handlelocationhidingandshowing = function (initialrun)
            {
                //alert(initialrun);
                if (initialrun != true)
                {
                    initialrun = false;
                }
                hidelocationcontrols(initialrun);
                switch ($("#id_organisation_type").val()) {
                    case 'TNRS'://residential institution
                    case 'TNCB':
                    case 'TNCM':
                    case 'TNND':
                    case 'TNGS':
                        $("#district_location").show();
                        $("#ward_location").show()
                        $("#community_location").show();
                        break;
                    case 'TNCW'://cwac
                        $("#district_location").show();
                        $("#ward_location").show()
                        break;
                    case 'TNGD':
                    case 'TNCD':
                    case 'TNNM':
                        $("#district_location").show();
                        break;
                }

                switch ($("#id_organisation_type").val()) {
                    case 'TNCB':
                        //case 'TNCW':
                        //case 'TNCM':
                    case 'TNRS'://residential institution
                    case 'TNND':
                    case 'TNNM':
                    case 'TNNH':
                        $("#div_id_legal_registration_type").show();
                        $("#div_id_legal_registratiom_number").show();
                        break;
                }
            };



            on_org_type_change = function () {
                var ismultiselect_district = true;
                var ismultiselect_ward = true;
                var ismultiselect_community = true;

                switch ($("#id_organisation_type").val()) {
                    case 'TNRS'://residential institution
                    case 'TNCB':
                    case 'TNCW'://cwac
                    case 'TNCD':
                    case 'TNGD':
                        ismultiselect_district = false;
                        ismultiselect_ward = false;
                        ismultiselect_community = false;
                        break;
                    case 'TNCM':
                    case 'TNGS':
                    case 'TNND':
                        //alert("some false");
                        ismultiselect_district = false;
                        ismultiselect_ward = true;
                        ismultiselect_community = true;
                    case 'TNNM':
                        ismultiselect_district = true;
                        ismultiselect_ward = false;
                        ismultiselect_community = false;
                        break;
                }


                $('#wards').multiSelect({
                    afterSelect: function (values) {
                        //$('#wards').attr('disabled', 'disabled');
                        //$("#wards").multiSelect("refresh");
                        $("#wards > option").each(function () {
                            if (values != this.value) {
                                if ($("#wards").attr('ismultiselect') != 'True') {
                                    switch ($("#id_organisation_type").val()) {
                                        case 'TNRS'://residential institution
                                        case 'TNCB':
                                        case 'TNCW'://cwac
                                        case 'TNGD':
                                            ismultiselect_district = false;
                                            ismultiselect_ward = false;
                                            ismultiselect_community = false;
                                            break;
                                        case 'TNCM':
                                        case 'TNGS':
                                        case 'TNND':
                                        case 'TNCD':
                                        case 'TNCM':
                                            //alert("some false");
                                            ismultiselect_district = false;
                                            ismultiselect_ward = true;
                                            ismultiselect_community = true;
                                            break;
                                        case 'TNNM':
                                            ismultiselect_district = true;
                                            ismultiselect_ward = false;
                                            ismultiselect_community = false;
                                            break;
                                    }
                                    if (ismultiselect_ward != true)
                                    {
                                        $(this).attr('disabled', 'disabled');
                                    }
                                    //alert(this.text + ' ' + this.value);
                                }
                            }
                        });
                        $("#wards").multiSelect("refresh");
                        $.ajax({
                            dataType: "json",
                            url: "/organisation/communities?ward_id=" + values,
                            contentType: "application/json; charset=utf-8",
                            //data: {"districtid" : values},
                            success: function (communities) {
                                var items = [];
                                $.each(communities, function (key, val) {
                                    $('#communities').multiSelect('addOption', {value: key, text: val});

                                });

                            }
                        });

                    },
                    afterDeselect: function (values) {
                        $("#wards > option").each(function () {

                            $(this).removeAttr('disabled')
                            //alert(this.text + ' ' + this.value);

                        });
                        $("#wards").multiSelect("refresh");
                        $.ajax({
                            dataType: "json",
                            url: "/organisation/communities?ward_id=" + values,
                            contentType: "application/json; charset=utf-8",
                            //data: {"districtid" : values},
                            success: function (communities) {
                                var items = [];
                                $.each(communities, function (key, val) {
                                    $("#communities option[value=" + key + "]").remove();
                                    $("#communities").multiSelect("refresh");
                                    //alert("Deselect value: "+key);
                                });
                            }
                        });
                        //$("#destination option[value='1']").remove();
                    }
                });
                $('#districts').multiSelect({
                    afterSelect: function (values) {
                        $("#districts > option").each(function () {
                            if (values != this.value) {
                                if ($("#districts").attr('ismultiselect') != 'True') {
                                    //alert(ismultiselect_district);
                                    switch ($("#id_organisation_type").val()) {
                                        case 'TNRS'://residential institution
                                        case 'TNCB':
                                        case 'TNCW'://cwac
                                        case 'TNGD':
                                            ismultiselect_district = false;
                                            ismultiselect_ward = false;
                                            ismultiselect_community = false;
                                            break;
                                        case 'TNCM':
                                        case 'TNGS':
                                        case 'TNND':
                                        case 'TNCD':
                                        case 'TNCM':
                                            //alert("some false");
                                            ismultiselect_district = false;
                                            ismultiselect_ward = true;
                                            ismultiselect_community = true;
                                            break;
                                        case 'TNNM':
                                            ismultiselect_district = true;
                                            ismultiselect_ward = false;
                                            ismultiselect_community = false;
                                            break;
                                    }
                                    if (ismultiselect_district != true)
                                    {

                                        $(this).attr('disabled', 'disabled');
                                    }
                                }
                                //alert(this.text + ' ' + this.value);
                            }
                        });
                        $("#districts").multiSelect("refresh");
                        $.ajax({
                            dataType: "json",
                            url: "/organisation/wards?district_id=" + values,
                            contentType: "application/json; charset=utf-8",
                            //data: {"districtid" : values},
                            success: function (wards) {
                                var items = [];
                                $.each(wards, function (key, val) {
                                    $('#wards').multiSelect('addOption', {value: key, text: val});

                                    //alert("Deselect value: "+key);
                                });
                            }
                        });

                        //$.getJSON( "/organisation/wards", {"districtid" : values}, function( data ) {
                        //	  var items = [];
                        //	  $.each( data, function( key, val ) {
                        //		  $('#wards').multiSelect('addOption', { value: key, text: val});
                        //		  //alert("Deselect value: "+key);
                        //	  });
                        //	});
                    },
                    afterDeselect: function (values) {
                        $("#districts > option").each(function () {

                            $(this).removeAttr('disabled')
                            //alert(this.text + ' ' + this.value);

                        });
                        $("#districts").multiSelect("refresh");
                        $.ajax({
                            dataType: "json",
                            url: "/organisation/wards?district_id=" + values,
                            contentType: "application/json; charset=utf-8",
                            //data: {"districtid" : values},
                            success: function (wards) {
                                var items = [];
                                $.each(wards, function (key, val) {
                                    $("#wards option[value=" + key + "]").remove();
                                    $("#wards").multiSelect("refresh");
                                    deselectcommunities(key);
                                    //alert("Deselect value: "+key);
                                });
                            }
                        });
                        //$("#destination option[value='1']").remove();
                    }
                });
                $('#communities').multiSelect({
                    afterSelect: function (values) {
                        $("#communities > option").each(function () {
                            if (values != this.value) {
                                if ($("#communities").attr('ismultiselect') != 'True') {
                                    //alert(ismultiselect_district);
                                    switch ($("#id_organisation_type").val()) {
                                        case 'TNRS'://residential institution
                                        case 'TNCB':
                                        case 'TNCW'://cwac
                                        case 'TNGD':
                                            ismultiselect_district = false;
                                            ismultiselect_ward = false;
                                            ismultiselect_community = false;
                                            break;
                                        case 'TNCM':
                                        case 'TNGS':
                                        case 'TNND':
                                        case 'TNCD':
                                        case 'TNCM':
                                            //alert("some false");
                                            ismultiselect_district = false;
                                            ismultiselect_ward = true;
                                            ismultiselect_community = true;
                                            break;
                                        case 'TNNM':
                                            ismultiselect_district = true;
                                            ismultiselect_ward = false;
                                            ismultiselect_community = false;
                                            break;
                                    }
                                    if (ismultiselect_community != true)
                                    {

                                        $(this).attr('disabled', 'disabled');
                                    }
                                }
                                //alert(this.text + ' ' + this.value);
                            }
                        });
                        $("#communities").multiSelect("refresh");
                    },
                    afterDeselect: function (values) {
                        $("#communities > option").each(function () {

                            $(this).removeAttr('disabled')
                            //alert(this.text + ' ' + this.value);

                        });
                        $("#communities").multiSelect("refresh");
                    }
                });
            };

            $("#id_organisation_type").change(handlelocationhidingandshowing);
            handlelocationhidingandshowing.apply($("#id_organisation_type"), [true]);


            function movieFormatResult(movie) {
                var markup = "<table class='movie-result'><tr>";
                if (movie.posters !== undefined && movie.posters.thumbnail !== undefined) {
                    markup += "<td valign='top'><img src='" + movie.posters.thumbnail + "' style='margin-right: 10px;' /></td>";
                }
                markup += "<td valign='top'><strong style='margin-bottom: 7px;'>" + movie.title + "</strong>";
                if (movie.critics_consensus !== undefined) {
                    markup += "<div class='movie-synopsis' style='font-size: 12px;'>" + movie.critics_consensus + "</div>";
                } else if (movie.synopsis !== undefined) {
                    markup += "<div class='movie-synopsis' style='font-size: 12px;'>" + movie.synopsis + "</div>";
                }
                markup += "</td></tr></table>"
                return markup;
            }

            $("#searchbtn").click(function () {
                //alert($("#searchorgtxt").val());
                $.ajax({
                    dataType: "html",
                    url: "/organisation/orglists?searchterm=" + $("#searchorgtxt").val(),
                    contentType: "application/json; charset=utf-8",
                    //data: {"districtid" : values},
                    success: function (results) {
                        if (results.length == 0) {
                            $('#orgresults tbody').html("<tr><td colspan='5'>No results for search term supplied</td></tr>");
                        }
                        else {
                            $('#orgresults tbody').empty().append(results);
                        }
                    },
                    error: function () {
                        $('#orgresults tbody').html("<tr><td span='5'>No results for search term supplied</td></tr>");
                        //alert($("#searchorgtxt").val())
                    }
                });
            });

            $(".gw-next").click(function () {
                var pagenumber = $(".gw-page").val();
                orgpagination(++pagenumber);
            });
            $(".gw-prev").click(function () {
                var pagenumber = $(".gw-page").val();
                orgpagination(--pagenumber);
            });
            orgpagination = function (pagenumber)
            {
                $(".totalrecords").empty().append("<i class='fa fa-spinner fa-spin'></i> Searching...");
                $(".searching").show();
                $(".totalrecords").hide();
                $.ajax({
                    dataType: "json",
                    url: "/organisation/orglists?searchterm=" + $("#searchorgtxt").val() + "&pagenumber=" + pagenumber + "&itemsperpage=" + $(".gw-pageSize").val() + "&orgtype=" + $("#orgtype").val() + "&search_all=" + $(".search_closed_org").is(':checked'),
                    success: function (results) {
                        if (results.length == 0) {
                            $('#orgresults tbody').html("<tr><td colspan='5'>No results for search term supplied</td></tr>");
                        }
                        else {
                            $('#orgresults tbody').empty().html("");
                            $.each(results, function (index) {

                                if (results[index].pageinfo == 'pageinfo')
                                {
                                    $(".countpages").empty().html(results[index].pagescount);
                                    $(".totalrecords").empty().append("Found " + results[index].total_records + " matching organisation(s)");

                                    //set the control number
                                    $(".gw-page").val(results[index].pagenumber);
                                    if (results[index].pagenumber >= results[index].pagescount)
                                    {
                                        if (!$(".gw-next").hasClass("disabled"))
                                        {
                                            $(".gw-next").addClass("disabled");
                                        }
                                        if ($(".gw-prev").hasClass("disabled"))
                                        {
                                            $(".gw-prev").removeClass("disabled");
                                        }

                                    }
                                    if (results[index].pagescount == 1) {
                                        if (!$(".gw-next").hasClass("disabled"))
                                        {
                                            $(".gw-next").addClass("disabled");
                                        }
                                        if (!$(".gw-prev").hasClass("disabled"))
                                        {
                                            $(".gw-prev").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber <= 1) {
                                        if (!$(".gw-prev").hasClass("disabled"))
                                        {
                                            $(".gw-prev").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount) {
                                        if ($(".gw-next").hasClass("disabled"))
                                        {
                                            $(".gw-next").removeClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount && results[index].pagenumber > 1) {
                                        if ($(".gw-prevt").hasClass("disabled"))
                                        {
                                            $(".gw-prev").removeClass("disabled");
                                        }
                                    }

                                }
                                else {

                                    var tmp_span = "<span class='badge label-danger'>closed</span>";
                                    var is_active = false;
                                    if (results[index].is_active === true)
                                    {
                                        tmp_span = "";
                                    }
                                    $("#orgresults > tbody").append("<tr class='table_row_clickable' " +
                                            //TODO implement security on lookup
                                            "href='/organisation/view_org?organisation_id=" + results[index].org_system_id + "' style='cursor:pointer'> " +
                                            "<td>" + results[index].org_id + " " + tmp_span + "</td> " +
                                            "<td>" + results[index].org_name + "</td> " +
                                            "<td>" + results[index].unit_type_name + "</td> " +
                                            "<td>" + results[index].parent_organisation_name + "</td> " +
                                            "<td>" + results[index].location + "</td> </tr>");
                                }
                            });
                        }
                    },
                    error: function () {
                        $('#orgresults tbody').html("<tr><td span='5'>No results for search term supplied</td></tr>");
                        //alert($("#searchorgtxt").val())
                    }
                });

                $(".searching").fadeOut('slow', function () {
                    $(".totalrecords").fadeIn('slow');
                });

                //$(".totalrecords").show();
            };
            $(".gw-pageSize").change(function () {
                $(".gw-pageSize").val($(".gw-pageSize").val());
                orgpagination(1);
            });
            $("#searchbtn2").click(function () {
                orgpagination(1);
            });

            $("#searchorgtxt").keypress(function (event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    orgpagination(1);
                    return false;
                }
            });

            function movieFormatSelection(movie) {
                return movie.title;
            }

            //==================Search Beneficiary start
            /*
             $("#searchbtnben").click(function(){
             //alert($("#searchorgtxt").val());
             $.ajax({
             dataType: "html",
             url: "/organisation/benlists?searchterm="+$("#searchbentxt").val(),
             contentType: "application/json; charset=utf-8",
             //data: {"districtid" : values},
             success: function( results ) {
             if (results.length == 0){
             $('#benresults tbody').html("<tr><td colspan='8'>No results for search term supplied</td></tr>");
             }
             else{
             $('#benresults tbody').empty().append(results);
             }
             },
             error: function () {
             $('#benresults tbody').html("<tr><td span='8'>No results for search term supplied</td></tr>");
             //alert($("#searchorgtxt").val())
             }
             });
             });
             */
            $(".gw-nextben").click(function () {
                var pagenumber = $(".gw-page").val();
                benpagination(++pagenumber);
            });
            $(".gw-prevben").click(function () {
                var pagenumber = $(".gw-page").val();
                benpagination(--pagenumber);
            });
            benpagination = function (pagenumber)
            {
                $(".totalrecords").empty().append("<i class='fa fa-spinner fa-spin'></i> Searching...");
                $(".searching").show();
                $(".totalrecords").hide();

                $.ajax({
                    dataType: "json",
                    url: "/organisation/benlists?searchterm=" + $("#searchbentxt").val() + "&pagenumber=" + pagenumber + "&itemsperpage=" + $(".gw-pageSizeben").val() + "&bentype=" + $("#bentype").val(),
                    success: function (results) {
                        if (results.length == 0) {
                            $('#benresults tbody').html("<tr><td colspan='8'>No results for search term supplied</td></tr>");
                        }
                        else {
                            $('#benresults tbody').empty().html("");
                            $.each(results, function (index) {

                                if (results[index].pageinfo == 'pageinfo')
                                {
                                    $(".countpages").empty().html(results[index].pagescount);
                                    $(".totalrecords").empty().append("Found " + results[index].total_records + " matching beneficiary(s)");

                                    //set the control number
                                    $(".gw-page").val(results[index].pagenumber);
                                    if (results[index].pagenumber >= results[index].pagescount)
                                    {
                                        if (!$(".gw-nextben").hasClass("disabled"))
                                        {
                                            $(".gw-nextben").addClass("disabled");
                                        }
                                        if ($(".gw-prevben").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").removeClass("disabled");
                                        }

                                    }
                                    if (results[index].pagescount == 1) {
                                        if (!$(".gw-nextben").hasClass("disabled"))
                                        {
                                            $(".gw-nextben").addClass("disabled");
                                        }
                                        if (!$(".gw-prevben").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber <= 1) {
                                        if (!$(".gw-prevben").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount) {
                                        if ($(".gw-nextben").hasClass("disabled"))
                                        {
                                            $(".gw-nextben").removeClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount && results[index].pagenumber > 1) {
                                        if ($(".gw-prevbent").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").removeClass("disabled");
                                        }
                                    }

                                }
                                else {
                                    $("#benresults > tbody").append("<tr class='table_row_clickable' " +
                                            "href='/beneficiary/view_ben?beneficiary_id=" + results[index].pk_id + "' style='cursor:pointer'> " +
                                            "<td>" + results[index].beneficiary_id + "</td> " +
                                            "<td>" + results[index].name + "</td> " +
                                            "<td>" + results[index].date_of_birth + "</td> " +
                                            "<td>" + results[index].sex + "</td> " +
                                            "<td>" + results[index].person_type + "</td> " +
                                            "<td>" + results[index].community + "</td> " +
                                            "<td>" + results[index].ward + "</td> " +
                                            "<td>" + results[index].district + "</td> " +
                                            "</tr>");
                                }
                            });
                        }
                    },
                    error: function () {
                        $('#benresults tbody').html("<tr><td span='8'>No results for search term supplied</td></tr>");
                        //alert($("#searchorgtxt").val())
                    }
                });
                //alert("success");
                $(".searching").fadeOut('slow', function () {
                    $(".totalrecords").fadeIn('slow');
                });

                //$(".totalrecords").show();
            };
            $(".gw-pageSizeben").change(function () {
                $(".gw-pageSizeben").val($(".gw-pageSizeben").val());
                benpagination(1);
            });
            $("#searchbtnben").click(function () {
                benpagination(1);
            });

            $("#searchbentxt").keypress(function (event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    benpagination(1);
                    return false;
                }
            });

            //==================Search Beneficiary End

            $(".select2-loading-remote-data").select2({
                placeholder: "Search for a movie",
                minimumInputLength: 1,
                ajax: {// instead of writing the function to execute the request we use Select2's convenient helper
                    url: "http://api.rottentomatoes.com/api/public/v1.0/movies.json",
                    dataType: 'jsonp',
                    data: function (term, page) {
                        return {
                            q: term, // search term
                            page_limit: 10,
                            apikey: "ju6z9mjyajq2djue3gbvv26t" // please do not use so this example keeps working
                        };
                    },
                    results: function (data, page) { // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to alter remote JSON data
                        return {
                            results: data.movies
                        };
                    }
                },
                initSelection: function (element, callback) {
                    // the input tag has a value attribute preloaded that points to a preselected movie's id
                    // this function resolves that id attribute to an object that select2 can render
                    // using its formatResult renderer - that way the movie name is shown preselected
                    var id = $(element).val();
                    if (id !== "") {
                        $.ajax("http://api.rottentomatoes.com/api/public/v1.0/movies/" + id + ".json", {
                            data: {
                                apikey: "ju6z9mjyajq2djue3gbvv26t"
                            },
                            dataType: "jsonp"
                        }).done(function (data) {
                            callback(data);
                        });
                    }
                },
                formatResult: movieFormatResult, // omitted for brevity, see the source of this page
                formatSelection: movieFormatSelection, // omitted for brevity, see the source of this page
                dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
                escapeMarkup: function (m) {
                    return m;
                } // we do not want to escape markup since we are displaying html in results
            });
            $(".select2-loading-data").select2({
                minimumInputLength: 1,
                query: function (query) {
                    var data = {results: []}, i, j, s;
                    for (i = 1; i < 5; i++) {
                        s = "";
                        for (j = 0; j < i; j++) {
                            s = s + query.term;
                        }
                        data.results.push({id: query.term + i, text: s});
                    }
                    query.callback(data);
                }
            });
            //END PLUGINS SELECT2

            //BEGIN PLUGINS BOOTSTRAP SELECT
            $('.selectpicker').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });
            //END PLUGINS BOOTSTRAP SELECT

            //BEGIN PLUGINS MULTI SELECT

            deselectcommunities = function (wardid)
            {

                //alert(wardid)
                $.ajax({
                    dataType: "json",
                    url: "/organisation/communities?ward_id=" + wardid,
                    contentType: "application/json; charset=utf-8",
                    //data: {"districtid" : values},
                    success: function (communities) {
                        var items = [];
                        $.each(communities, function (key, val) {
                            $("#communities option[value=" + key + "]").remove();
                            $("#communities").multiSelect("refresh");
                            //alert("Deselect value: "+key);
                        });
                    }
                });
            };


            $('#workforce_ward').multiSelect();
            $('#workforce_district').multiSelect({
                afterSelect: function (values) {
                    $.ajax({
                        dataType: "json",
                        url: "/organisation/wards?district_id=" + values,
                        contentType: "application/json; charset=utf-8",
                        //data: {"districtid" : values},
                        success: function (wards) {
                            var items = [];
                            $.each(wards, function (key, val) {
                                $('#workforce_ward').multiSelect('addOption', {value: key, text: val});
                                //alert("Deselect value: "+key);
                            });
                        }
                    });

                    //$.getJSON( "/organisation/wards", {"districtid" : values}, function( data ) {
                    //	  var items = [];
                    //	  $.each( data, function( key, val ) {
                    //		  $('#wards').multiSelect('addOption', { value: key, text: val});
                    //		  //alert("Deselect value: "+key);
                    //	  });
                    //	});
                },
                afterDeselect: function (values) {
                    $.ajax({
                        dataType: "json",
                        url: "/organisation/wards?district_id=" + values,
                        contentType: "application/json; charset=utf-8",
                        //data: {"districtid" : values},
                        success: function (wards) {
                            var items = [];
                            $.each(wards, function (key, val) {
                                $("#workforce_ward option[value=" + key + "]").remove();
                                $("#workforce_ward").multiSelect("refresh");
                                //alert("Deselect value: "+key);
                            });
                        }
                    });
                    //$("#destination option[value='1']").remove();
                }
            });
            $('#workforce_community').multiSelect();

            $('#callbacks').multiSelect({
                afterSelect: function (values) {
                    alert("Select value: " + values);
                },
                afterDeselect: function (values) {
                    alert("Deselect value: " + values);
                }
            });
            $('#keep-order').multiSelect({keepOrder: true});
            $('#public-methods').multiSelect();
            $('#select-all').click(function () {
                $('#public-methods').multiSelect('select_all');
                return false;
            });
            $('#deselect-all').click(function () {
                $('#public-methods').multiSelect('deselect_all');
                return false;
            });
            var arr = [];

            for (var i = 0; i < 20; i++) {
                arr[i] = 'elem_' + (i + 1);
            }
            $('#select-20').click(function () {
                $('#public-methods').multiSelect('select', arr);
                return false;
            });
            $('#deselect-20').click(function () {
                $('#public-methods').multiSelect('deselect', arr);
                return false;
            });
            $('#refresh').on('click', function () {
                $('#public-methods').multiSelect('refresh');
                return false;
            });
            $('#add-option').on('click', function () {
                $('#public-methods').multiSelect('addOption', {value: 21, text: 'test 21', index: 0});
                return false;
            });
            $('#optgroup').multiSelect({selectableOptgroup: true});
            //END PLUGINS MULTI SELECT

            $(".autocompleteovc").autocomplete({
                source: function (request, response) {
                    $.getJSON("/workforce/orgautocomp?org_name=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value[1],
                                value: value
                            };

                        }));
                    });
                },
                minLength: 1,
                delay: 100,
                select: function (event, ui) {
                    // feed hidden org name field
                    $('#org_unit_name_hidden').val(ui.item.value[1]);
                    // feed org id hidden field
                    $('#org_unit_id_hidden').val(ui.item.value[0]);

                },
            });

            $(".autocompleteben").autocomplete({
                source: function (request, response) {
                    $.getJSON("/beneficiary/ben_autocomplete?ben_name=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value,
                                value: value
                            };

                        }));
                    });
                },
                minLength: 1,
                delay: 100,
                select: function (event, ui) {
                    //feed nrc hidden field
                    $('#ben_wfc_name_national_id').val(ui.item.value[1]);
                    // feed hidden org name field
                    $('#ben_name_hidden').val(ui.item.value[2]);
                    // feed org id hidden field
                    $('#ben_id_hidden').val(ui.item.value[0]);

                },
            });

            $(".autocompletewfc").autocomplete({
                source: function (request, response) {
                    $.getJSON("/beneficiary/ben_wfc_autocomplete?wfc_name=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value,
                                value: value
                            };

                        }));
                    });
                },
                minLength: 1,
                delay: 100,
                select: function (event, ui) {
                    //feed nrc hidden field
                    $('#ben_wfc_name_national_id').val(ui.item.value[2]);
                    // feed hidden workforce field
                    $('#ben_wfc_name_hidden').val(ui.item.value[1]);
                    // feed workforce id hidden field
                    $('#ben_wfc_id_hidden').val(ui.item.value[0]);

                },
            });

            $(".autocompleteRes").autocomplete({
                source: function (request, response) {
                    $.getJSON("/beneficiary/ben_res_autocomplete?ins_name=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value[0],
                                value: value
                            };

                        }));
                    });
                },
                minLength: 1,
                delay: 100,
                select: function (event, ui) {
                    $('#id_ben_res_name_hidden').val(ui.item.value[0]);
                    var vals = ui.item.value[1];
                    $('#id_res_id_hidden').val(vals[0]);

                    $('#id_ben_district').val('' + vals[1] + '')
                    handledistrictchange(vals[2]);
                    //alert(vals[3]);
                    handlewardchange(vals[3], vals[2]);
                },
            });

            handledistrictchange = function (selval) {
                $.ajax({
                    dataType: "json",
                    url: "/beneficiary/wards?district_id=" + $("#id_ben_district").val(),
                    contentType: "application/json; charset=utf-8",
                    success: function (wards) {
                        $("#id_ben_ward").empty();
                        $("#id_community").empty();
                        $('<option>').val(0).text('-----').appendTo("#id_ben_ward");
                        $.each(wards, function (key, val) {
                            $('<option>').val(key).text(val).appendTo("#id_ben_ward");
                            if (selval != null)
                            {
                                if ($('#id_ben_ward').val() != selval)
                                {
                                    $('#id_ben_ward').val('' + selval + '');
                                }
                            }
                        });
                    }
                });
            };

            handlewardchange = function (selval, wardval) {
                searchval = $("#id_ben_ward").val();
                if (wardval != null)
                {
                    searchval = wardval;
                }
                $.ajax({
                    dataType: "json",
                    url: "/beneficiary/communities?ward_id=" + searchval,
                    contentType: "application/json; charset=utf-8",
                    success: function (wards) {
                        $("#id_community").empty();
                        $('<option>').val(0).text('-----').appendTo("#id_community");
                        $.each(wards, function (key, val) {
                            $('<option>').val(key).text(val).appendTo("#id_community");
                            if (selval != null)
                            {
                                //alert(selval);
                                if ($('#id_community').val() != selval)
                                {
                                    $('#id_community').val('' + selval + '');
                                }
                            }
                        });
                    }
                });
            };


            $("#id_ben_district").change(handledistrictchange);

            $("#id_ben_ward").change(handlewardchange);

            $('#id_workforce_type').change(function () {
                if ($('#id_workforce_type').val() == "TWVL") {
                    $('#div_id_steps_ovc_number').show();
                    $('#div_id_sign_number').hide();
                    $('#div_id_ts_number').hide();
                    $('#div_id_man_number').hide();
                } else if ($('#id_workforce_type').val() == "TWGE") {
                    $('#div_id_steps_ovc_number').hide();
                    $('#div_id_sign_number').show();
                    $('#div_id_ts_number').show();
                    $('#div_id_man_number').show();
                } else if ($('#id_workforce_type').val() == "TWNE") {
                    $('#div_id_steps_ovc_number').hide();
                    $('#div_id_sign_number').hide();
                    $('#div_id_ts_number').hide();
                    $('#div_id_man_number').hide();
                } else {
                    $('#div_id_steps_ovc_number').hide();
                    $('#div_id_sign_number').hide();
                    $('#div_id_ts_number').hide();
                    $('#div_id_man_number').hide();
                }
                clearControls();
            });
            function clearControls() {
                $('#id_steps_ovc_number').attr('value', '');
                $('#id_ts_number').attr('value', '');
                $('#id_sign_number').attr('value', '');
                $('#id_man_number').attr('value', '');
            }

            if ($('#id_workforce_type').val() == "TWVL") {
                $('#div_id_steps_ovc_number').show();
                $('#div_id_sign_number').hide();
                $('#div_id_ts_number').hide();
                $('#div_id_man_number').hide();
            } else if ($('#id_workforce_type').val() == "TWGE") {
                $('#div_id_steps_ovc_number').hide();
                $('#div_id_sign_number').show();
                $('#div_id_ts_number').show();
                $('#div_id_man_number').show();
            } else if ($('#id_workforce_type').val() == "TWNE") {
                $('#div_id_steps_ovc_number').hide();
                $('#div_id_sign_number').hide();
                $('#div_id_ts_number').hide();
                $('#div_id_man_number').hide();
            } else {
                $('#div_id_steps_ovc_number').hide();
                $('#div_id_sign_number').hide();
                $('#div_id_ts_number').hide();
                $('#div_id_man_number').hide();
            }

            populateOrgTable();
            function populateOrgTable() {
                if ($("#id_org_data_hidden").length > 0) {
                    var hiddenText = $("#id_org_data_hidden").val();
                    //,,org_id:U0000034,org_name:cwaac example,primary_org:Yes,wra:,,org_id:U0000026,org_name:Ndu ya ngu,primary_org:No,wra:,,org_id:U0000018,org_name:Mwandi home based,primary_org:No,wra:
                    if (hiddenText == '')
                        return;
                    $("#orgTable > tbody").html("");
                    var splitOrgs = (hiddenText).split(',,');
                    //org_id:U0000034,org_name:cwaac example,primary_org:Yes,wra:
                    for (index = 0; index < splitOrgs.length; ++index) {
                        var org_id = '';
                        var org_name = '';
                        var primary_org_unit = '';
                        var wra = '';
                        //alert(index);
                        if (splitOrgs[index] == "") {
                            continue;
                        }
                        var kvp = splitOrgs[index].split(',');

                        //org_id:U0000034            
                        //org_name:cwaac example
                        //primary_org:Yes,wra:
                        for (i = 0; i < kvp.length; ++i) {
                            var kvpr = kvp[i].split(':');
                            if (kvpr.length == 2) {
                                if (kvpr[0] == 'org_id') {
                                    org_id = kvpr[1];
                                }
                                if (kvpr[0] == 'org_name') {
                                    org_name = kvpr[1];
                                }
                                if (kvpr[0] == 'primary_org') {
                                    primary_org_unit = kvpr[1];
                                }
                                if (kvpr[0] == 'wra') {
                                    wra = kvpr[1];
                                }
                            }
                        }

                        var hide = $("#id_reg_assistant_col1").is(":hidden");
                        var hidden = 'text';
                        if (hide) {
                            alert(hide);
                            hidden = '';
                        }
                        $("#orgTable").find('tbody')
                                .append($('<tr>')
                                        .append($('<td id="0">')
                                                .append($('<label>')
                                                        .text('')
                                                        .css('width', '1%')
                                                        )
                                                )
                                        .append($('<td id="1">')
                                                .append($('<label>')
                                                        .text(org_name)
                                                        )
                                                )
                                        .append($('<td id="2">')
                                                .append($('<label>')
                                                        .text(org_id)
                                                        )
                                                )
                                        .append($('<td id="3">')
                                                .append($('<label>')
                                                        .text(primary_org_unit)
                                                        )
                                                )
                                        .append($('<td id="4">')
                                                .append($('<label>')
                                                        .text(wra)
                                                        .css("visibility", hidden)
                                                        )
                                                )
                                        .append($("<td id='5'> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>"))
                                        );
                    }
                }
            }

            $("#wfcsearchbtn").click(function () {
                wfcpagination(1);
            });
            var today = new Date();
            var month = new Array();
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
            var currentMonth = month[today.getMonth()];
            var formattedDate = (today.getDate() + '-' + currentMonth + '-' + today.getFullYear());


            $("#id_workforce_type_change_date").val(formattedDate);
            $("#id_parent_org_change_date").val(formattedDate);
            $("#id_work_locations_change_date").val(formattedDate);

            var myDate = new Date();
            var validDate = (myDate.getMonth() + 1) + '/' + myDate.getDate() + '/' + myDate.getFullYear();

            if ($("#id_guardian_change_date").val() == "")
            {
                $("#id_guardian_change_date").val(formattedDate);
            }

            if ($("#id_location_change_date").val() == "")
            {
                $("#id_location_change_date").val(formattedDate);

            }

            if ($("#id_date_paper_form_filled").val() == "")
            {
                $("#id_date_paper_form_filled").val(formattedDate);
            }

            //$( ".datepicker" ).datepicker( "option", "dateFormat", 'd-MM-yy' );
            //$( ".datepicker" ).datepicker( "option", "changeMonth", true );
            //$( ".datepicker" ).datepicker( "option", "changeYear", true );

            wfcpagination = function (pagenumber)
            {
                $(".totalrecords").empty().append("<i class='fa fa-spinner fa-spin'></i> Searching...");
                $(".searching").show();
                $(".totalrecords").hide();

                $.ajax({
                    dataType: "json",
                    url: "/workforce/wfclists?searchterm=" + $("#searchwfctxt").val() + "&pagenumber=" + pagenumber + "&itemsperpage=" + $(".gw-pageSize").val() + "&wfctype=" + $("#wfctype").val(),
                    success: function (results) {
                        if (results.length == 0) {
                            $('#wfcresults tbody').html("<tr><td colspan='5'>No results for search term supplied</td></tr>");
                        }
                        else {
                            $('#wfcresults tbody').empty().html("");
                            $.each(results, function (index) {

                                if (results[index].pageinfo == 'pageinfo')
                                {
                                    $(".countpages").empty().html(results[index].pagescount);
                                    $(".totalrecords").empty().append("Found " + results[index].total_records + " matching member(s)");

                                    //set the control number
                                    $(".gw-page").val(results[index].pagenumber);
                                    if (results[index].pagenumber >= results[index].pagescount)
                                    {
                                        if (!$(".gw-nextben").hasClass("disabled"))
                                        {
                                            $(".gw-nextben").addClass("disabled");
                                        }
                                        if ($(".gw-prevben").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").removeClass("disabled");
                                        }

                                    }
                                    if (results[index].pagescount == 1) {
                                        if (!$(".gw-nextben").hasClass("disabled"))
                                        {
                                            $(".gw-nextben").addClass("disabled");
                                        }
                                        if (!$(".gw-prevben").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber <= 1) {
                                        if (!$(".gw-prevben").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount) {
                                        if ($(".gw-nextben").hasClass("disabled"))
                                        {
                                            $(".gw-nextben").removeClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount && results[index].pagenumber > 1) {
                                        if ($(".gw-prevbent").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").removeClass("disabled");
                                        }
                                    }

                                }
                                else {
                                    var action_html = "";
                                    var row_html = "<tr>";
                                    if (!results[index].is_capture_app) {
                                        row_html = "<tr class='" + results[index].user_can_view + "' " +
                                                "href='/workforce/view_wfc?person_id_int=" + results[index].id_int + "' style='" + results[index].user_can_view_cursor + "'> "
                                    }
                                    if (!results[index].is_capture_app) {
                                        action_html = "<td><span style=\"display:" + results[index].user_can_edit + "\"><a href='/workforce/update_wfc?person_id=" + results[index].id_int + "' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-edit\"></i>&nbsp;Update</a></span>&nbsp;<span style=\"display:" + results[index].user_can_assign_role + "\"><a href='/allocate_role?workforceid=" + results[index].id_int + "' class=\"btn btn-warning btn-xs\"><i class=\"fa fa-lock\"></i>&nbsp;Allocate role</a></span></td> </tr>";
                                    }
                                    $("#wfcresults > tbody").append(row_html +
                                            "<td>" + results[index].user_id + "</td> " +
                                            "<td>" + results[index].name + "</td> " +
                                            "<td>" + results[index].person_type + "</td> " +
                                            "<td>" + results[index].primary_org_unit_name + "</td> " +
                                            "<td>" + results[index].location + "</td> " +
                                            action_html
                                            );
                                }
                            });
                        }
                    },
                    error: function () {
                        $('#wfcresults tbody').html("<tr><td span='6'>No results for search term supplied</td></tr>");
                        //alert($("#searchorgtxt").val())
                    }
                });
                $(".searching").fadeOut('slow', function () {
                    $(".totalrecords").fadeIn('slow');
                });


                //$(".totalrecords").show();
            };

            $("#uploadformbtn").click(function () {
                begin_upload();
            });


            begin_upload = function ()
            {
                $.ajax({
                    dataType: "json",
                    url: "/api/begin_upload",
                    success: function (results) {
                        if (results.length == 0) {
                            $('#formtbl tbody').empty().html("");
                            $('#formtbl').hide();
                            $('#emptyuploadqueue').val("There are currently no forms in the upload queue ");
                            $('#emptyuploadqueue').show();
                        }
                        else {
                            $('#emptyuploadqueue').hide()
                            $('#formtbl tbody').empty().html("");
                            $.each(results, function (index) {
                                $("#formtbl > tbody").append(
                                        "<tr>" +
                                        "<td>" + results[index].formtype + "</td> " +
                                        "<td>" + results[index].count + "</td> " +
                                        "</tr>"
                                        );
                            });
                            $('#formtbl tbody').show();
                        }
                    },
                    error: function () {
                        alert('error');
                        $('#emptyuploadqueue').val("An error occured. Please try again ");
                        $('#emptyuploadqueue').show();
                        $('#formtbl tbody').empty().html("");
                        $('#formtbl').hide();
                    }
                });
            };

            $("#downloadbtn").click(function () {
                begin_download();
            });
            
            begin_download = function ()
            {
                $.ajax({
                    dataType: "json",
                    url: "/api/begin_download?section=" + $("#id_download_sections").val(),
                    success: function (results) {
                        if (results.length == 0) {
                            $('#downloadstbl tbody').empty().html("");
                            //$('#downloadstbl').hide();
                        }
                        else {
                            $('#downloadstbl tbody').empty().html("");
                            $.each(results, function (index) {
                                $("#downloadstbl > tbody").append(
                                        "<tr>" +
                                            "<td>" + results[index].section + "</td> " +
                                            "<td>" + results[index].started + "</td> " +
                                            "<td>" + results[index].ended + "</td> " +
                                            "<td>" + results[index].num_of_recs + "</td> " +
                                            "<td>" + results[index].success + "</td> " +
                                        "</tr>"
                                        );
                            });
                            $('#downloadstbl tbody').show();
                        }
                    },
                    error: function () {
                        alert('error');
                        $('#downloadstbl tbody').empty().html("");
                        //$('#downloadstbl').hide();
                    }
                });
            };
        });
