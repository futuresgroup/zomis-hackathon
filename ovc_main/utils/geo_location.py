from ovc_main.models import SetupGeorgraphy, RegOrgUnitGeography, RegOrgUnit
from django.db.models import Q
import operator

def get_children_locations_id(location_level, location_id):
    pass

def get_parent_locations_id(location_level, location_id):
    pass

def get_grandparent_locations_id(location_level, location_id):
    pass

def get_grandchildren_locations_id(location_level, location_id):
    pass

def get_locations(location_level):
    pass

def geo_type(geo_id):
    pass

def get_geo_ancestors(geo_id, arr=[], limit_level='province'):
    geoobj = SetupGeorgraphy.objects.get(area_id=geo_id)
    if geoobj.area_type_id == limit_level:
        arr = [(geo_id,geoobj.area_name)] + arr
        return arr
    else:
        arr = [(geo_id,geoobj.area_name)] + arr
        return get_geo_ancestors(geoobj.parent_area_id, arr)

def get_geo_ancestors_level_info(geo_id, arr=[], limit_level='province'):
    geoobj = SetupGeorgraphy.objects.get(area_id=geo_id)
    if geoobj.area_type_id == limit_level:
        arr = [(geo_id,geoobj.area_name, geoobj.area_type_id)] + arr
        return arr
    else:
        arr = [(geo_id,geoobj.area_name, geoobj.area_type_id)] + arr
        return get_geo_ancestors_level_info(geoobj.parent_area_id, arr)
    
def get_geo_ancestors_ids(geo_id, arr=[], limit_level='province'):
    geoobj = SetupGeorgraphy.objects.get(area_id=geo_id)
    if geoobj.area_type_id == limit_level:
        arr = [geo_id] + arr
        return arr
    else:
        arr = [geo_id] + arr
        return get_geo_ancestors_ids(geoobj.parent_area_id, arr)


class geoinfo(object):
    geoid = None
    geoname = None
    
    def __init__(self, geoid, geoname):
        self.geoid = geoid
        self.geoname = geoname
        
    def __str__(self):
        return 'ID: %s, Name: %s' % (self.geoid, self.geoname)

class communityinfo(geoinfo):
    geo_parent_info = None
    def __init__(self, geoid, geoname, geo_parent_info):
        super(communityinfo,self).__init__(geoid, geoname)
        self.geo_parent_info = geo_parent_info
        
def get_community_ward(communityid):
    
    _communityinfo = None
    comm = RegOrgUnit.objects.get(pk=communityid)
    com_areas = RegOrgUnitGeography.objects.filter(organisation__pk=communityid, is_void=False).values_list('area_id', flat=True)
    if com_areas:
        wards = SetupGeorgraphy.objects.filter(area_id__in=com_areas, area_type_id='ward').values()
        for ward in wards:
            ginfo = geoinfo(ward['area_id'], ward['area_name'])
            _communityinfo = communityinfo(communityid, comm.unit_name,ginfo )
            
    return _communityinfo
    
    
def get_community_parent(communityid):
    pass
        
def matches_for_display(geo_list, communities=[]):
    ancestorsparentlist = []
    to_display= []
    
    communitiesinfo = {}
    for communityid in communities:
        communityinfo = get_community_ward(communityid)
        if communityinfo and communityinfo.geo_parent_info.geoid in communitiesinfo:
            communitiesinfo[communityinfo.geo_parent_info.geoid].append(communityinfo)
        elif communityinfo:
            communitiesinfo[communityinfo.geo_parent_info.geoid] = [communityinfo]
    for geo_id in geo_list:
        if geo_id in ancestorsparentlist:
            continue
        ancestors = get_geo_ancestors(geo_id)
        ancestorlength = len(ancestors)
        
        if ancestorlength > 1:
            for parentindex in range(0, ancestorlength-1):
                parentid = ancestors[parentindex][0]
                if parentid in ancestorsparentlist:
                    continue
                ancestorsparentlist.append(parentid)
        toaddtodisplay = []
        for ancestor in ancestors:
            geoobj = geoinfo(ancestor[0], ancestor[1])
            toaddtodisplay.append(geoobj)
        
        community = None
        searchgeoid = geo_id
        if communitiesinfo and searchgeoid in communitiesinfo and len(communitiesinfo[searchgeoid]) > 0:
            for community in communitiesinfo[searchgeoid]:
                to_display.append(toaddtodisplay + [community])
        else:
            to_display.append(toaddtodisplay)
    return to_display

def get_communities_in_ward(wardid):
    orgs = [(org.organisation.id, org.organisation.unit_id, org.organisation.unit_name) for org in RegOrgUnitGeography.objects.filter(area_id__in=wardid,organisation__is_gdclsu=True, is_void=False)]
    return orgs

def search_org_in_location(location_term):
    print location_term
    org_ids = None
    search_condition = []
    for term in location_term:
        search_condition.append(Q(area_name__icontains=term.lower()))

    geos = SetupGeorgraphy.objects.filter(reduce(operator.or_, search_condition)).values_list('area_id', 'area_name')

    if geos:
        idstosearch = []
        for geo_id, geo_name in geos:
            if geo_id in idstosearch:
                continue
            idstosearch.append(geo_id)
            childrenids = get_children_ids(geo_id)
            idstosearch = idstosearch + childrenids
        
        org_ids = RegOrgUnitGeography.objects.filter(area_id__in=idstosearch, is_void=False).values_list('organisation', flat=True)

    return org_ids

def get_children_ids(geoid, geoids=[]):
    geoids = [] + geoids
    children_ids = SetupGeorgraphy.objects.filter(parent_area_id=geoid).values_list('area_id', flat=True)
    if children_ids:
        for childid in children_ids:
            if childid in geoids:
                continue
            geoids.append(childid)
            get_children_ids(childid, geoids)
    return geoids

def get_geo_info(geoids):
    geos = [(geo.area_id, geo.area_name) for geo in SetupGeorgraphy.objects.filter(area_id__in=geoids)]
    return geos
        
        
def get_orgid_subunits_and_self(geoid, geoids=[]):
    geoids = [geoid] + geoids
    children_ids = RegOrgUnit.objects.filter(parent_org_unit_id=geoid).values_list('pk', flat=True)
    if children_ids:
        for childid in children_ids:
            if childid in geoids:
                continue
            geoids.append(childid)
            get_children_ids(childid, geoids)
    return geoids

        
        
        
                