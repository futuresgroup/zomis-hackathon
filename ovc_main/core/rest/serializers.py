from django.forms import widgets
from rest_framework import serializers
from ovc_main.models import *

class RegPersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegPerson
        fields = (  
                    'id',
                    'beneficiary_id',
                    'workforce_id',
                    'birth_reg_id',
                    'national_id',
                    'first_name',
                    'other_names',
                    'surname',
                    'date_of_birth',
                    'date_of_death',
                    'sex_id',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        person = None
        if len(RegPerson.objects.filter(pk=id)) > 0:
            person = RegPerson.objects.get(pk=id)
        else:
            person = RegPerson()
        person.id=id
        person.beneficiary_id=validated_data['beneficiary_id']
        person.workforce_id=validated_data['workforce_id']
        person.birth_reg_id=validated_data['birth_reg_id']
        person.national_id=validated_data['national_id']
        person.first_name=validated_data['first_name']
        person.other_names=validated_data['other_names']
        person.surname=validated_data['surname']
        person.date_of_birth=validated_data['date_of_birth']
        person.date_of_death=validated_data['date_of_death']
        person.sex_id=validated_data['sex_id']
        person.is_void=validated_data['is_void']
        
        if person:
            person.save()
        return person

class RegPersonsGeoSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegPersonsGeo
        fields = (  
                    'id',
                    'person',
                    'area_id',
                    'date_linked',
                    'date_delinked',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        person_geo = None
        if len(RegPersonsGeo.objects.filter(pk=id)) > 0:
            person_geo = RegPersonsGeo.objects.get(pk=id)
        else:
            person_geo = RegPersonsGeo()
        person_geo.id=id
        person_geo.person=validated_data['person']
        person_geo.area_id=validated_data['area_id']
        person_geo.date_linked=validated_data['date_linked']
        person_geo.date_delinked=validated_data['date_delinked']
        person_geo.is_void=validated_data['is_void']
        
        if person_geo:
            person_geo.save()
        return person_geo

class RegPersonsGdclsuSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegPersonsGdclsu
        fields = (  
                    'id',
                    'person',
                    'gdclsu',
                    'date_linked',
                    'date_delinked',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        person_gdclsu = None
        if len(RegPersonsGdclsu.objects.filter(pk=id)) > 0:
            person_gdclsu = RegPersonsGdclsu.objects.get(pk=id)
        else:
            person_gdclsu = RegPersonsGdclsu()
        person_gdclsu.id=id
        person_gdclsu.person=validated_data['person']
        person_gdclsu.gdclsu=validated_data['gdclsu']
        person_gdclsu.date_linked=validated_data['date_linked']
        person_gdclsu.date_delinked=validated_data['date_delinked']
        person_gdclsu.is_void=validated_data['is_void']
        
        if person_gdclsu:
            person_gdclsu.save()
        return person_gdclsu
                  
class RegPersonsOrgUnitsSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegPersonsOrgUnits
        fields = (  
                    'id',
                    'person',
                    'parent_org_unit',
                    'primary',
                    'date_linked',
                    'date_delinked',
                    'is_void'
                  )

    def save(self, validated_data, id):
        person_orgs = None
        if len(RegPersonsOrgUnits.objects.filter(pk=id)) > 0:
            person_orgs = RegPersonsOrgUnits.objects.get(pk=id)
        else:
            person_orgs = RegPersonsOrgUnits()
        person_orgs.id=id
        person_orgs.person=validated_data['person']
        person_orgs.parent_org_unit=validated_data['parent_org_unit']
        person_orgs.primary=validated_data['primary']
        person_orgs.date_linked=validated_data['date_linked']
        person_orgs.date_delinked=validated_data['date_delinked']
        person_orgs.is_void=validated_data['is_void']
        
        if person_orgs:
            person_orgs.save()
        return person_orgs
    
class RegPersonsTypesSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegPersonsTypes
        fields = (  
                    'id',
                    'person',
                    'person_type_id',
                    'date_began',
                    'date_ended',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        person_types = None
        if len(RegPersonsTypes.objects.filter(pk=id)) > 0:
            person_types = RegPersonsTypes.objects.get(pk=id)
        else:
            person_types = RegPersonsTypes()
        person_types.id=id
        person_types.person=validated_data['person']
        person_types.person_type_id=validated_data['person_type_id']
        person_types.date_began=validated_data['date_began']
        person_types.date_ended=validated_data['date_ended']
        person_types.is_void=validated_data['is_void']
        
        if person_types:
            person_types.save()
        return person_types
                  
class FormsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Forms
        fields = (  
                    'form_id',
                    'form_type_id',
                    'form_subject_id',
                    'form_area_id',
                    'date_began',
                    'date_ended',
                    'date_filled_paper',
                    'person_id_filled_paper',
                    'org_unit_id_filled_paper',
                    'capture_site_id',
                    'timestamp_created',
                    'person_id_created',
                    'timestamp_updated',
                    'person_id_updated',
                    'is_void'
                  )

class RegOrgUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegOrgUnit
        fields = (  
                    'id',
                    'unit_id',
                    'unit_name',
                    'unit_type_id',
                    'is_gdclsu',
                    'date_operational',
                    'date_closed',
                    'parent_org_unit_id',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        org_unit = None
        if len(RegOrgUnit.objects.filter(pk=id)) > 0:
            org_unit = RegOrgUnit.objects.get(pk=id)
        else:
            org_unit = RegOrgUnit()
        org_unit.id=id
        org_unit.unit_id=validated_data['unit_id']
        org_unit.unit_name=validated_data['unit_name']
        org_unit.unit_type_id=validated_data['unit_type_id']
        org_unit.is_gdclsu=validated_data['is_gdclsu']
        org_unit.date_operational=validated_data['date_operational']
        org_unit.date_closed=validated_data['date_closed']
        org_unit.parent_org_unit_id=validated_data['parent_org_unit_id']
        org_unit.is_void=validated_data['is_void']
        
        if org_unit:
            org_unit.save()
        return org_unit
    
class RegOrgUnitGeoSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegOrgUnitGeography
        fields = (  
                    'id',
                    'organisation',
                    'area_id',
                    'date_linked',
                    'date_delinked',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        org_unit_geo = None
        if len(RegOrgUnitGeography.objects.filter(pk=id)) > 0:
            org_unit_geo = RegOrgUnitGeography.objects.get(pk=id)
        else:
            org_unit_geo = RegOrgUnitGeography()
        org_unit_geo.id=id
        org_unit_geo.organisation=validated_data['organisation']
        org_unit_geo.area_id=validated_data['area_id']
        org_unit_geo.date_linked=validated_data['date_linked']
        org_unit_geo.date_delinked=validated_data['date_delinked']
        org_unit_geo.is_void=validated_data['is_void']
        
        if org_unit_geo:
            org_unit_geo.save()
        return org_unit_geo
    
class SetupListSerializer(serializers.ModelSerializer):
    class Meta:
        model = SetupList
        fields = (  
                    'id',
                    'item_id',
                    'item_description',
                    'item_description_short',
                    'item_category',
                    'the_order',
                    'user_configurable',
                    'sms_keyword',
                    'field_name',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        setup_list = None
        if len(SetupList.objects.filter(pk=id)) > 0:
            setup_list = SetupList.objects.get(pk=id)
        else:
            setup_list = SetupList()
        setup_list.id=id
        setup_list.item_id=validated_data['item_id']
        setup_list.item_description=validated_data['item_description']
        setup_list.item_description_short=validated_data['item_description_short']
        setup_list.item_category=validated_data['item_category']
        setup_list.the_order=validated_data['the_order']
        setup_list.user_configurable=validated_data['user_configurable']
        setup_list.sms_keyword=validated_data['sms_keyword']
        setup_list.field_name=validated_data['field_name']
        setup_list.is_void=validated_data['is_void']
        
        if setup_list:
            setup_list.save()
        return setup_list

class SetupGeoSerializer(serializers.ModelSerializer):
    class Meta:
        model = SetupGeorgraphy
        fields = (  
                    'id',
                    'area_id',
                    'area_type_id',
                    'area_name',
                    'parent_area_id',
                    'area_name_abbr',
                    'timestamp_created',
                    'timestamp_updated',
                    'is_void'
                  )
                  
    def save(self, validated_data, id):
        setup_geo = None
        if len(SetupGeorgraphy.objects.filter(pk=id)) > 0:
            setup_geo = SetupGeorgraphy.objects.get(pk=id)
        else:
            setup_geo = SetupGeorgraphy()
        setup_geo.id=id
        setup_geo.area_id=validated_data['area_id']
        setup_geo.area_type_id=validated_data['area_type_id']
        setup_geo.area_name=validated_data['area_name']
        setup_geo.parent_area_id=validated_data['parent_area_id']
        setup_geo.area_name_abbr=validated_data['area_name_abbr']
        setup_geo.is_void=validated_data['is_void']
        
        if setup_geo:
            setup_geo.save()
        return setup_geo
